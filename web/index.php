<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 09/02/2017
 * Time: 17:37
 */

use Silex\Application;
use Symfony\Component\Yaml\Yaml;
use thvc\Bootstrap\Bootstrapper;
use thvc\Database\DbConnection;
use thvc\Model\RecipientRepo;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

// Bootstrapping:
$bootstrapper = new Bootstrapper($app);
$bootstrapper->routingBootstrapper();

// Services:

$app['config'] = Yaml::parse(file_get_contents("../config/config.yml"));

$app['database_connection'] = function(Application $app) {
    return new DbConnection (
        $app['config']['db_config']['db_host'],
        $app['config']['db_config']['db_user'],
        $app['config']['db_config']['db_pass'],
        $app['config']['db_config']['db_name'],
        $app['config']['db_config']['charset']

    );
};

$app['recipient_repository'] = function(Application $app) {
    return new RecipientRepo($app['database_connection']->newDbConnection());
};



















$app->run();