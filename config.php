<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 13/02/2017
 * Time: 17:15
 */

/* By Qassim Hassan, wp-time.com */

$scope = "https://www.googleapis.com/auth/userinfo.email"; // Do not change it!

$redirect_uri = "xxxx"; // Enter your redirect_uri

$client_id = "xxxx"; // Enter your client_id

$client_secret = "xxxx"; // Enter your client_secret

$login_url = "https://accounts.google.com/o/oauth2/v2/auth?scope=$scope&response_type=code&redirect_uri=$redirect_uri&client_id=$client_id"; // Do not change it!

$image_size = 100; // Change user profile image size: 100 = 100x100

?>