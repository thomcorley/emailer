<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 13/02/2017
 * Time: 17:16
 */

session_start();

/* By Qassim Hassan, wp-time.com */

include 'config.php';
if( isset($_SESSION["gp_access_token"]) ){ // If logged in
    include 'Qassim_HTTP.php';
    $access_token = $_SESSION["gp_access_token"]; // User access token
    $api_url = "https://www.googleapis.com/plus/v1/people/me?fields=aboutMe%2Cemails%2Cimage%2Cname&access_token=$access_token"; // Do not change it!

    if( !isset($_SESSION["gp_result"]) ){
        $result = Qassim_HTTP(0, $api_url, 0, 0);
        $_SESSION["gp_result"] = $result;
        $user_info = $_SESSION["gp_result"];
    }else{
        $user_info = $_SESSION["gp_result"];
    }

    $first_name = $user_info['name']['givenName']; // User first name
    $last_name = $user_info['name']['familyName']; // User last name
    $email = $user_info['emails'][0]['value']; // User email
    $get_profile_image = $user_info['image']['url'];
    $change_image_size = str_replace("?sz=50", "?sz=$image_size", $get_profile_image);
    $profile_image_link = $change_image_size; // User profile image link
    $page_title = "Hello $first_name $last_name!"; // Page title if user is logged in
}
else{
    $page_title = "Login With Google Plus Using Qassim_HTTP() Function"; // Page title if user is no logged in
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
</head>
<body>

<?php

if( isset($_SESSION["gp_access_token"]) ){ // If logged in
    ?>
    <img src="<?php echo $profile_image_link; ?>">
    <h1>Hello <?php echo $first_name.' '.$last_name; ?>!</h1>
    <p>Your Email: <?php echo $email; ?></p>
    <p><a href="logout.php">Logout</a></p>
    <?php
}

else{ // If is not logged in
    ?>
    <h1>Welcome</h1>
    <p><a href="<?php echo $login_url; ?>">Login with Google plus</a></p>
    <?php
}

?>

</body>
</html>