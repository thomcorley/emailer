<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 13/02/2017
 * Time: 17:17
 */

session_start();

/* By Qassim Hassan, wp-time.com */

if ( isset($_SESSION["gp_access_token"]) or isset($_SESSION["gp_result"]) ){
    unset($_SESSION["gp_access_token"]);
    unset($_SESSION["gp_result"]);
    header("location: index.php");
}

else{
    header("location: index.php");
}

?>