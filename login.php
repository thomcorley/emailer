<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 13/02/2017
 * Time: 17:16
 */

session_start();

/* By Qassim Hassan, wp-time.com */

if( !isset($_GET['code']) or isset($_SESSION["gp_access_token"]) or isset($_SESSION["gp_result"]) ){
    header("location: index.php");
    exit();
}

include 'Qassim_HTTP.php';
include 'config.php';

$header = array( "Content-Type: application/x-www-form-urlencoded" );

$data = http_build_query(
    array(
        'code' => str_replace("#", null, $_GET['code']),
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'redirect_uri' => $redirect_uri,
        'grant_type' => 'authorization_code'
    )
);

$url = "https://www.googleapis.com/oauth2/v4/token";

$result = Qassim_HTTP(1, $url, $header, $data);

if( !empty($result['error']) ){ // If error login
    header("location: index.php");
    exit();
}else{
    $_SESSION["gp_access_token"] = $result['access_token']; // Access Token
    header("location: index.php");
}

?>