<?php

namespace thvc\Model;

/**
 * Recipient
 *
 * @author Thomas Corley
 * @package thvc\Model
 */
class Recipient
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $recipient;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $businessName;

    /**
     * Recipient constructor.
     * @param $id
     * @param string $recipient
     * @param string $emailAddress
     * @param string $businessName
     */
    public function __construct($id, $recipient, $emailAddress, $businessName)
    {
        $this->recipient = $recipient;
        $this->emailAddress = $emailAddress;
        $this->businessName = $businessName;
        $this->id = $id;
    }


}
