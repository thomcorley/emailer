<?php

namespace thvc\Model;

/**
 * Email
 *
 * @author Thomas Corley
 * @package thvc\Model
 */
class Email
{
    /**
     * @var Recipient
     */
    private $recipient;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $content;

    /**
     * Email constructor.
     * @param $recipient
     * @param $subject
     * @param $content
     */
    public function __construct(Recipient $recipient, $subject, $content)
    {
        $this->recipient = $recipient;
        $this->subject = $subject;
        $this->content = $content;
    }


}
