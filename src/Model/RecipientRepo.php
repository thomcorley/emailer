<?php

namespace thvc\Model;

/**
 * EmailRepository
 *
 * @author Thomas Corley
 * @package thvc\Model
 */
class RecipientRepo

{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * RecipientRepo constructor.
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return Recipient[]
     */
    public function getAll()
    {
        $statement = $this->db->prepare('
            SELECT
                name AS name,
                email_address AS email,
                business_name AS business,
                id AS id
            FROM email_addresses
        ');

        $statement->execute();
        $recipientRow = $statement->fetchAll();
        $recipient = [];
        foreach ($recipientRow as $array){
            $recipient[] = new Recipient(
                $array['id'],
                $array['name'],
                $array['email'],
                $array['business']
            );
        }
        return $recipient; // Array of Recipient Objects
    }

}
