<?php

namespace thvc\Database;

use PDO;

/**
 * DbConnection
 *
 * @author Thomas Corley
 * @package thvc\Database
 */
class DbConnection
{
    /**
     * @var string
     */
    private $dbHost;
    /**
     * @var string
     */
    private $dbUser;
    /**
     * @var string
     */
    private $dbPass;
    /**
     * @var string
     */
    private $dbName;
    /**
     * @var string
     */
    private $charset;

    /**
     * DbConnection constructor.
     * @param string $dbHost
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbName
     * @param $charset
     */
    public function __construct($dbHost, $dbUser, $dbPass, $dbName, $charset)
    {
        $this->dbHost = $dbHost;
        $this->dbUser = $dbUser;
        $this->dbPass = $dbPass;
        $this->dbName = $dbName;
        $this->charset = $charset;
    }


    /**
     * @return PDO $db
     */
    public function newDbConnection()
    {
        $dataSourceName = "mysql:host={$this->dbHost};dbname={$this->dbName};charset={$this->charset}"; // This is known as the DSN
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        try {
            return $db = new PDO($dataSourceName, $this->dbUser, $this->dbPass, $options);
        } catch (\PDOException $e) {
            echo 'Error : ' . $e->getMessage();
        }

    }
}
