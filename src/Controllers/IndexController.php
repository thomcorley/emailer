<?php

namespace thvc\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use thvc\Model\Recipient;

/**
 * IndexController
 *
 * @author Thomas Corley
 * @package thvc\Controllers
 */
class IndexController
{
    /**
     * @param Application $app
     * @return Recipient[]
     */
    public function index(Application $app)
    {
        $arrayOfRecipientObjects =$app['recipient_repository']->getAll();

        return $arrayOfRecipientObjects;


    }

}
