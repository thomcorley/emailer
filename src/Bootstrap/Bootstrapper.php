<?php

namespace thvc\Bootstrap;

use Silex\Application;
use Symfony\Component\Yaml\Yaml;


/**
 * Bootstrapper
 *
 * @author Thomas Corley
 * @package thvc\Bootstrapper
 */
class Bootstrapper
{
    /**
     * @var Application
     */
    private $app;

    /**
     * Bootstrapper constructor.
     * @param $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }


    /**
     * @return void
     */

    public function routingBootstrapper()
    {
        $yaml = Yaml::parse(file_get_contents('../config/routes.yml'));

        foreach ($yaml as $routeArray){
            switch ($routeArray['request_method']){
                case 'get':
                    $this->app->get($routeArray['route_URL'], $routeArray['controller']);
                    break;
                case 'post':
                    $this->app->post($routeArray['route_URL'], $routeArray['controller']);
                    break;
                case 'put':
                    $this->app->put($routeArray['route_URL'], $routeArray['controller']);
                    break;

            }


        }


    }
}
